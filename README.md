Simple code-server that is configured with its defaults.
Make sure to create a volume before running the container.
Lastly, no Cuda drivers are present with the current image itself meaning nvidia-smi --list-gpus works as intended but it is missing python3 and any ML packages which you might be using, including headers.
